const startBtn = document.getElementById('start-btn');
const stScreen = document.getElementById('start-screen');
const nameBtn = document.getElementById('name-btn');
const nameScreen = document.getElementById('name-screen');
const closeBtn = document.getElementById('close-btn');
const greeting = document.getElementById('greeting-screen');
const player = document.getElementById('player-name');
const message = document.getElementById('message');
const game = document.getElementById('game-screen');
const cursor = document.querySelector('.cursor');
const audio = document.getElementById('audio');
const play = document.querySelector('.play');
const stBtn = document.getElementById('start');

let score = 0;
let winScore = 20;
let timeLimit = 60000;
let startTime = Date.now();
let gameOver = false;
let birdCount = 0;
let movementSpeed = 50;

function getSpeed(){
    movementSpeed = document.querySelector('input[type = radio]:checked').value;
}



window.addEventListener('mouseover', function(e){
      cursor.style.left = `${e.pageX}px`;
      cursor.style.top = `${e.pageY}px`;
})

function toggleSound () {
    let audio = document.getElementById('bg-audio');
    let img = document.getElementById('sound-toggle');

    if(audio.paused){
        audio.play();
        img.src = 'images/soundon.png';
    }
    else{
        audio.pause();
        img.src = 'images/soundoff.png';
    }
}

function startScreen(){
    startBtn.classList.add('hidden');
    nameScreen.classList.remove('hidden');
    nameScreen.style.display = 'flex';
    closeBtn.classList.remove('hidden');
}

function closeScreen(){
    closeBtn.classList.add('hidden');
    nameScreen.classList.add('hidden');
    nameScreen.style.display = 'none';
    startBtn.classList.remove('hidden');
}

function greetingScreen() {
    const playerName = player.value;
    message.innerText = `Hello ${playerName} \n Have a Nice game`;
    nameScreen.classList.add('hidden');
    nameScreen.style.display = 'none';
    stScreen.classList.add('hidden');
    game.classList.remove('hidden');
    greeting.classList.remove('hidden');
}


function createBird(){
    const bird = document.createElement('img');
    bird.src = 'images/normal6.gif';
    bird.classList.add('bird');
    let birdWidth = Math.floor(Math.random() * 50 + 50);
    let birdHeight = Math.floor(Math.random() * 50 + 50); 
    let randomTop = Math.random() * 250 + 150;

    bird.style.left = `0px`;
    bird.style.top = `${randomTop}px`;
    bird.style.width = `${birdWidth}px`;
    bird.style.height =  `${birdHeight}px`;

    bird.addEventListener('click', handleBirdClick);
    document.getElementById('birds').appendChild(bird);
}


function move() {
    const birds = document.querySelectorAll('.bird');
    birds.forEach(bird => {
        const currentLeft = parseInt(bird.style.left);
        const width = parseInt(bird.style.width); 
        const newLeft = currentLeft + (Math.random() * movementSpeed + 40);

        if (newLeft + width  >= window.innerWidth) {
            bird.style.left = `${window.innerWidth}px`;
            setTimeout(() => {
                bird.remove(); 
            }, 320); 
        } else {
            bird.style.left = `${newLeft}px`;
        }
    });
}

function handleBirdClick(event) {
    audio.play();
    const bird = event.target;
    bird.remove();
    score++;
    document.getElementById('score').innerText = `Score: ${score}`;
    createExplosion(event.clientX, event.clientY);
}

function createExplosion(x, y) {
    explosion = document.createElement('img');
    explosion.src = 'images/explosion2.gif'
    explosion.classList.add('explosion');
    explosion.style.left = `${x-50}px`;
    explosion.style.top = `${y-50}px`;
    setTimeout(() => explosion.remove(), 500);
    document.getElementById('explosions').appendChild(explosion);
}

function updateGameStatus() {
    if (score >= winScore) {
        gameOver = true;
        message.innerText = `Congratulations You Win! \n Play Again?`;
        greeting.style.display = 'flex';
        stBtn.classList.add('hidden');
        play.classList.remove('hidden');
        greeting.classList.remove('hidden');
    }

    if (Date.now() - startTime > timeLimit) {
        gameOver = true;
        message.innerText = `You Lose the game! \n Play Again?`;
        greeting.style.display = 'flex';
        stBtn.classList.add('hidden');
        play.classList.remove('hidden');
        greeting.classList.remove('hidden');
    }
}

function startGame() {
    greeting.classList.add('hidden');
    greeting.style.display = 'none';

    setInterval(() => {
        if (!gameOver) {
            createBird();
        }
    }, 1000); 
    

    setInterval(() => {
        if (!gameOver) {
            requestAnimationFrame(move);
            updateGameStatus();
        }
    }, 400);
}



