



const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const collisionCanvas = document.getElementById('collisionCanvas');
const collisionCTX= collisionCanvas.getContext('2d');
collisionCanvas.width = window.innerWidth;
collisionCanvas.height = window.innerHeight;

const cursorImage = new Image();
cursorImage.src = 'images/crosshair.png';

let score = 0;
let gameOver = false;
let winScore = 20;
let timeLimit = 30000;
let startTime = Date.now();

ctx.font = '35px Impact';

let timeToNextBird = 0;
let birdInterval = 700;
let lastTime = 0;


document.addEventListener('mousemove', e => {
    cursorImage.style.left = `${e.clientX}px`;
    cursorImage.style.top = `${e.clientY}px`;
});

cursorImage.onload = () => {
    cursorImage.style.position = 'absolute';
    cursorImage.style.pointerEvents = 'none';

    cursorImage.style.width = '50px';
    cursorImage.style.height = 'auto'; 

    document.body.appendChild(cursorImage);

    document.body.style.cursor = 'none';
};

let birds = [];
class Bird{
    constructor(){
        this.spriteWidth = 406;
        this.spriteHeight = 368;
        this.sizeModifier = Math.random() * 0.28+ 0.22;
        this.width = this.spriteWidth * this.sizeModifier;
        this.height = this.spriteHeight * this.sizeModifier;
        this.x = 0;
        this.y = Math.random() * (canvas.height - this.height)
        this.directionX = Math.random() * 0.3 + 2;
        this.directionY = Math.random() * 4 - 1.5;
        this.markedForDeletion = false;
        this.image = new Image();
        this.image.src = 'images/normal6.PNG';
        this.frame = 0;
        this.maxFrame = 7;
        this.timeSinceFlap = 0;
        this.flapInterval = Math.random()* 30 + 30;
        this.randomColors = [Math.floor(Math.random()* 256), Math.floor(Math.random()* 256), Math.floor(Math.random()* 256)];
        this.color = 'rgb(' + this.randomColors[0] + ',' + this.randomColors[1] + ',' + this.randomColors[2] +')';
    }
    update(deltatime){
        if(this.y < 0 || this.y > canvas.height - this.height){
            this.directionY = this.directionY * -1;
        }
        this.x += this.directionX;
        this.y += this.directionY;
        if(this.x > canvas.width){
            this.markedForDeletion = true
        }

        this.timeSinceFlap += deltatime;
        
        if(this.timeSinceFlap > this.flapInterval){
            if(this.frame > this.maxFrame) {
                this.frame = 0;
            }
            else{
                this.frame ++;
            }
            this.timeSinceFlap = 0;
        }
        if(this.x < 0 - this.width){
            gameOver = true;
        }

    }
    draw(){
        collisionCTX.fillStyle = this.color;
        collisionCTX.fillRect(this.x, this.y, this.width, this.height);
        ctx.drawImage(this.image, this.frame * this.spriteWidth, 0, this.spriteWidth, this.spriteHeight, this.x, this.y, this.width, this.height);
    }
}

let explosions = [];
class Explosion {
    constructor(x, y, size){
        this.image = new Image();
        this.image.src = 'images/explosion2.PNG';
        this.spriteWidth = 537;
        this.spriteHeight = 700;
        this.size = size;
        this.x = x;
        this.y = y;
        this.frame = 0;
        this.sound = new Audio ();
        this.sound.src = 'audio/bomb.mp3';
        this.timeSinceLastFrame = 0;
        this.frameInterval = 200;
        this.markedForDeletion = false;
    }
    update(deltatime){
        if(this.frame === 0){
            this.sound.play();
        }
        this.timeSinceLastFrame += deltatime;
        if(this.timeSinceLastFrame > this.frameInterval){
            this.frame ++;
            if(this.frame > 5){
                this.markedForDeletion = true;
            }
        }
    }
    draw(){
        ctx.drawImage(this.image, this.frame * this.spriteWidth, 0, this.spriteWidth, this.spriteHeight, this.x, this.y, this.size, this.size);
    }
}


function drawScore(){
    ctx.fillStyle = 'black'
    ctx.fillText('Score: '+score , 95, 70);
    ctx.fillStyle = 'white'
    ctx.fillText('Score: '+score , 100, 70);

}

function updateGameStatus() {
    ctx.textAlign = 'center'
    ctx.fillStyle = 'black'
    if (score >= winScore) {
        gameOver = true;
        ctx.fillText('Congratulations! You win', canvas.width/2, canvas.height/2);
        
    }


    if (Date.now() - startTime > timeLimit) {
        gameOver = true;
        ctx.fillText('Game over! You lose!', canvas.width/2, canvas.height/2);
    }
}


window.addEventListener('click', function(e){
    const detectPixelColor = collisionCTX.getImageData(e.x,e.y, 1, 1);
    const pc = detectPixelColor.data;
    birds.forEach(object => {
        if(object.randomColors[0] === pc[0] && object.randomColors[1] === pc[1] &&object.randomColors[2] === pc[2] ){

            object.markedForDeletion = true;
            score ++;
            explosions.push(new Explosion(object.x, object.y, object.width));
        }
    })
})

function animate(timestamp){
    
    collisionCTX.clearRect(0, 0, canvas.width, canvas.height);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    let deltatime = timestamp - (lastTime);
    lastTime = timestamp;
    timeToNextBird += deltatime;
    if(timeToNextBird > birdInterval){
        birds.push(new Bird());
        timeToNextBird = 0;
        birds.sort(function(a, b){
            return a.width - b.width;
        });
    };
    drawScore();
    [...birds,...explosions].forEach(object => object.update(deltatime));
    [...birds,...explosions].forEach(object => object.draw());
    birds = birds.filter(object => !object.markedForDeletion
        );
    explosions = explosions.filter(object => !object.markedForDeletion);

    if (!gameOver) { 
        requestAnimationFrame(animate);
        updateGameStatus();
    }
    else{
        updateGameStatus();
    }
    
}

animate(0);