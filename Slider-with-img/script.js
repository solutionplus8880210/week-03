let imageSlide = document.getElementById('rotator');

let maxImages = 35;
let currentImage = 1;
let autoPlayInterval;
let randomPlayInterval;
let partData;

getData();

function changeImage(next) {
    currentImage += next;
    if (currentImage > maxImages) {
        currentImage = 1;
    } else if (currentImage < 1) {
        currentImage = maxImages;
    }
    imageSlide.src = `img/${currentImage}.jpg`;
    renderData(currentImage);
}

function startAutoPlay() {
     autoPlayInterval = setInterval(() => {
        changeImage(1);
    }, 2000); 
}

function stopAutoPlay (){
    clearInterval(autoPlayInterval);
}

function randomSlide (){
    randomPlayInterval = setInterval(() => {
        let randomIndex = Math.floor(Math.random() * maxImages);
        changeImage(randomIndex);
    }, 2000);
}

function stopRandomAutoPlay() {
    clearInterval(randomPlayInterval);
}
async function getData() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const data = await response.json();

        if (data.length > maxImages) {
            partData = data.slice(0, maxImages);
            renderData(currentImage);
        }
    } catch (error) {
        console.error('error:', error);
    }
}
function renderData(index) {
    if (partData && partData[index - 1]) {
        const ID = document.getElementsByClassName('id')[0];
        const title = document.getElementsByClassName('title')[0];
        const body = document.getElementsByClassName('body')[0];
        
        ID.innerText = `ID : ${partData[index - 1].id}`;
        title.innerText = `Title : ${partData[index - 1].title}`;
        body.innerText = `Body : ${partData[index - 1].body}`;
    }
}