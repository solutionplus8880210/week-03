let current = 1;
let autoPlayInterval;
let randomPlayInterval;
let data;

getData();

async function getData(){
    try{
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        data = await response.json();
        renderData(0)
    }catch(error){
        console.log('error : ',error);
    }
}

function renderData(index){

    current += index;

    if(current > data.length){
        current = 1;
    }else if(current < 1){
        current = data.length;
    }

    const ID = document.getElementsByClassName('id')[0];
    const title = document.getElementsByClassName('title')[0];
    const body = document.getElementsByClassName('body')[0];

    ID.innerText = `ID : ${data[current - 1].id}`;
    title.innerText = `Title : ${data[current - 1].title}`;
    body.innerText =   data[current -1 ].body;
}

function startAutoPlay(){
    autoPlayInterval = setInterval(() => {
        renderData(1);
    }, 2000); 
}

function randomSlide() {
    randomPlayInterval = setInterval(() => {
        let randomIndex = Math.floor(Math.random() * data.length);
        renderData(randomIndex);
    },2000);
}

function stopAutoPlay() {
    clearInterval(autoPlayInterval);
}

function stopRandom() {
    clearInterval(randomPlayInterval);
}